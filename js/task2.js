
function startTask2()
{
    let number = prompt('Введи целое число не равное 0'),
    numbers = '',
    simpleNums = '',
    answer = 'Ничего не работает!'

    // console.log(isNaN(number),isNaN(+number))

    while(isNaN(number) || +number % 1 !== 0 || +number === 0)
    {
        number = prompt('Так а целое число введи!',number)
    }
    
    number = +number
    if(number > 0)
    {
        for(let i = 0; i <= number; i++)
        {
            if(i !== 0 && i % 5 === 0)
            {
                numbers += `${i}, `
            }

            // простые числа
            let simpleNum = returnTrueLastEdition(i)
            if(simpleNum)
            {
                simpleNums += `${simpleNum}, `
            }
        }
    }
    else
    {
        for(let i = 0; i >= number; i--)
        {
            if(i !== 0 && i % 5 === 0)
            {
                numbers += `${i}, `
            }
            
            // простые числа
            let simpleNum = returnTrueLastEdition(i)
            if(simpleNum)
            {
                simpleNums += `${simpleNum}, `
            }
        }
    }
    answer = `ты ввел ${number}, результат:\n`
    answer = (numbers === '') ? 'Sorry, no numbers % 5' : `Числа кратные 5 такие: ${numbers}`
    answer += (simpleNums) ? `\nПростые счисла ${simpleNums}` : `\nПростых чисел нет!`
    alert(answer)
}

function additionalTast2()
{
    let m = prompt('Введи стартовое число'), n = prompt('Введи финишное число'),
        numbers = '',
        simpleNums = '',
        answer = 'Ничего не работает!'

    // console.log(isNaN(m) || isNaN(n))

    while(isNaN(m) || isNaN(n) || +n === +m || +m % 1 !== 0 || +n % 1 !== 0)
    {
        if(isNaN(m) || isNaN(n) || +m % 1 !== 0 || +n % 1 !== 0)
        {
            alert(`Значения "${m}" и "${n}" должны быть целыми числами!`)
        }
        else if(+n === +m)
        {
            alert(`Значения "${m}" и "${n}" НЕ должны быть равны!`)
        }
        m = prompt('Введи стартовое число числом')
        n = prompt('Введи финишное число тоже числом')
    }
    // console.log(+m,+n)

    m = +m
    n = +n
    if(n > m)
    {
        for(let i = m; i <= n; i++)
        {
            console.log(m,n,i)

            if(i !== 0 && i % 5 === 0)
            {
                numbers += `${i}, `
            }
            // простые числа
            let simpleNum = returnTrueLastEdition(i)
            if(simpleNum)
            {
                simpleNums += `${simpleNum}, `
            }
        }
    }
    else if(n < m)
    {
        for(let i = m; i >= n; i--)
        {
            console.log(m,n,i)
            if(i !== 0 && i % 5 === 0)
            {
                numbers += `${i}, `
            }
              // простые числа
              let simpleNum = returnTrueLastEdition(i)
              if(simpleNum)
              {
                  simpleNums += `${simpleNum}, `
              }
        }
    }
    answer = `ты ввел ${m} и ${n}, результат:\n`
    answer += (numbers === '') ? 'Sorry, no numbers % 5' : `Числа кратные 5 такие: ${numbers}`
    answer += (simpleNums) ? `\nПростые счисла ${simpleNums}` : `\nПростых чисел нет!`
    alert(answer)
    
}

function returnTrueLastEdition(i)
{
    let isSimpleflag = true

    iAbs = Math.abs(i)

    // исключаем 1, потому что оно не является простым
    if(iAbs <= 1)
    {
        return
    }

    // console.log('abs',Math.abs(i))

    for(let k = iAbs - 1; 1 < k; k-- )
    {
        // console.log('i, k',iAbs,k)
        
        if(iAbs % k === 0)
        {
            isSimpleflag = false
        }
    }        

    if (isSimpleflag === true) 
    {
        // console.log('simple',i)
        return i
    }
    return
}